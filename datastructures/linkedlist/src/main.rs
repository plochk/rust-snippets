struct Node {
    value: i32,
    next: Option<Box<Node>>,
    previous: Option<Box<Node>>,
}

fn main() {
    let mut head: Node = Node {
        value: 1,
        next: None,
        previous: None,
    };
    let node2: Node = Node {
        value: 5,
        next: None,
        previous: None,
    };
    head.next = Some(Box::new(node2));
}
