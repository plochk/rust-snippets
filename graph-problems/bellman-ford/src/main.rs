#[derive(Debug)]
struct Edge {
    from: u8,
    to: u8,
    cost: i32,
}

fn main() {
    let vertices: [u8; 3] = [0, 1, 2];
    let edges: [Edge; 3] = [
        Edge {
            from: 0,
            to: 1,
            cost: 2,
        },
        Edge {
            from: 0,
            to: 2,
            cost: 4,
        },
        Edge {
            from: 1,
            to: 2,
            cost: 1,
        },
    ];
    let res = bellman_ford(vertices, edges, 0);
    println!("{:?}", res);
}

fn bellman_ford(
    vertices: [u8; 3],
    edges: [Edge; 3],
    source_node: u8,
) -> ([Option<u8>; 3], [i32; 3]) {
    // Init
    let mut distances: [i32; 3] = [99, 99, 99]; // 99 = infinite
    let mut predecessors: [Option<u8>; 3] = [None, None, None];
    distances[usize::from(source_node)] = 0;

    // |V|-times, check for each edge whether it is a path improvement.
    let vertice_amount: usize = vertices.len();
    for _ in 0..vertice_amount {
        for edge in &edges {
            let from: usize = usize::try_from(edge.from).unwrap();
            let to: usize = usize::try_from(edge.to).unwrap();
            // Check if this edge would be an improvement to our current path
            // to this vertice.
            if distances[from] + edge.cost < distances[to] {
                distances[to] = distances[from] + edge.cost;
                predecessors[to] = Some(edge.from);
            }
        }
    }
    return (predecessors, distances);
}
